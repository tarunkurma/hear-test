package org.example.hear.api.jokes.categories;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.example.hear.api.data.Categories;
import org.example.hear.api.data.ChuckNorris;
import org.example.hear.api.jokes.Joker;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CategoryTest {

    private final Joker joker = new Joker();
    private static final String SQL_INJECTION = " OR 1=1 ";
    private static final String ERROR_MESSAGE = "search.query: size must be between 3 and 120";

    @BeforeClass
    void setup() {
        RestAssured.baseURI = "https://api.chucknorris.io";
    }

    /**
     * The following tests would be modified to compare the response of the API,
     * with the Categories from a data source, like a DB/File/API using a DAO class
     * to set expected Categories list.
     * <p>
     * In lieu of the data source, a default list was used in Categories class
     */
    @Test
    void testGetCategories() {
        Assert.assertEquals(joker.getJokesCategories().getCategories(), new Categories().getCategories());
    }

    /**
     * Following test conditions would be tested:
     * (1) Exact keyword match
     * (2) Case insensitive: lower case
     * (3) Case insensitive: upper case
     * (4) Starts with
     * (5) Ends with
     * (6) Negative test: GUID that does not match any strings
     * (7) Security test: SQL Injection with an OR is true condition
     *
     * @return list of valid keywords
     */
    @DataProvider
    Iterator<Object[]> validSearchKeywords() {
        List<Object[]> keywords = new ArrayList<>();

        String keyword = joker
                .getJokesCategories()
                .getCategories()
                .stream()
                .filter(word -> word.length() > 3)
                .findAny()
                .orElseThrow(() -> new RuntimeException("Error getting Joke Categories"));

        List<String> categories = new ArrayList<>();
        categories.add(keyword);
        categories.add(keyword.toLowerCase());
        categories.add(keyword.toUpperCase());
        categories.add(StringUtils.substring(keyword, 0, keyword.length() - 1));
        categories.add(StringUtils.substring(keyword, 1, keyword.length()));
        categories.add("0e3e0884-0c2f-4953-8150-9f579e4f186a");
        categories.add(keyword + SQL_INJECTION);

        categories.forEach(category -> keywords.add(new Object[]{category}));

        return keywords.iterator();
    }

    @Test(dataProvider = "validSearchKeywords")
    void testSearchQuery(String key) {
        assertSearchContainsKeyWord(joker.getJokesSearch(key), key);
    }

    /**
     * Following test conditions would be tested:
     * (1) Boundary Condition Test: String with length = 1 to test > 3 chars required as query parameter
     * (2) Boundary Condition Test: String with length = 2 to test > 3 chars required as query parameter
     * (3) Boundary Condition Test: String with length = 121 to test max 120 chars as query parameter
     * (4) Negative test: null
     * (5) Negative test: empty
     * (6) Negative test: SPACE ' '
     *
     * @return list of invalid keywords
     */
    @DataProvider
    Iterator<Object[]> invalidSearchKeywords() {
        List<Object[]> keywords = new ArrayList<>();
        List<String> invalidValues = new ArrayList<>();
        invalidValues.add("a");
        invalidValues.add("aa");
        invalidValues.add(StringUtils.repeat("a", 121));
        invalidValues.add(null);
        invalidValues.add("");
        invalidValues.add(StringUtils.SPACE);

        invalidValues.forEach(category -> keywords.add(new Object[]{category}));

        return keywords.iterator();
    }

    @Test(dataProvider = "invalidSearchKeywords")
    void testSearchQueryError(String key) {
        Response response = joker.query("jokes/search", key);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.SC_BAD_REQUEST);

        Assert.assertEquals(response.getBody().jsonPath().getString("message"), ERROR_MESSAGE);

    }

    /**
     * This method checks if Categories OR value contains the keyword, if neither are true, a failure is recorded.
     *
     * @param chuckNorrises deserialized 'result' json array from API response body
     * @param keyword       search keyword
     */
    private void assertSearchContainsKeyWord(List<ChuckNorris> chuckNorrises, String keyword) {
        //Null keyword should return an empty result
        if (keyword == null) {
            Assert.assertTrue(chuckNorrises.isEmpty());
        }

        SoftAssert softAssert = new SoftAssert();

        chuckNorrises.forEach(chuckNorris -> {
            if (!chuckNorris.getCategories().contains(keyword) && !chuckNorris.getValue().toUpperCase().contains(keyword.toUpperCase())) {
                softAssert.fail(String.format("[%s] was not found in: ", keyword) + chuckNorris.toString());
            }
        });

        softAssert.assertAll();
    }

}
