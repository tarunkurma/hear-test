package org.example.hear.ui.windows;

import org.example.hear.ui.drivers.BrowserDriver;
import org.example.hear.ui.pages.WindowsPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WindowsPageTest {

    private WebDriver driver;
    private WindowsPage windowsPage;

    @BeforeClass
    void loadWindowsPage() {
        this.driver = new BrowserDriver().getDriver();
        this.windowsPage = new WindowsPage(driver);
        windowsPage.launchWindowsPage();
    }

    @Test
    void testNewPage() {
        windowsPage.clickHere();
        windowsPage.switchToNewPage();
        Assert.assertEquals(windowsPage.getNewPageHeaderText(), "New Window");
    }

    @AfterClass
    void tearDown() {
        driver.close();
        driver.quit();
    }
}
