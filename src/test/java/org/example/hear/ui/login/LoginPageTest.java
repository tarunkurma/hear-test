package org.example.hear.ui.login;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.example.hear.ui.drivers.BrowserDriver;
import org.example.hear.ui.pages.LoginPage;
import org.example.hear.ui.pages.SecureAreaPage;

import java.util.*;

import static org.example.hear.ui.pages.SecureAreaPage.SECURE_MSG;

public class LoginPageTest {

    private final WebDriver driver = new BrowserDriver().getDriver();
    private final LoginPage loginPage = new LoginPage(driver);
    private final SecureAreaPage secureAreaPage = new SecureAreaPage(driver);

    //Credentials would ideally be stored in a static secret management software like - Vault or AWS Secrets Manager
    private static final String USERNAME = "tomsmith";
    private static final String PASSWORD = "SuperSecretPassword!";

    //List of values for error condition
    private static final List<String> values = Arrays.asList(null, "", StringUtils.SPACE, "Incorrect");


    @Test
    void testSuccessfulLogin() {
        loginPage.launchLoginPage();
        loginPage.loginActions(USERNAME, PASSWORD);

        Assert.assertEquals(secureAreaPage.getSecureAreaMessage(), SECURE_MSG);

        secureAreaPage.logout();
    }

    /**
     * Provides a collection of error or invalid entries for User Name and Password fields.
     *
     * @return a value from values list
     */
    @DataProvider(name = "errorValues")
    Iterator<Object[]> getErrorValues() {
        List<Object[]> errorValues = new ArrayList<>();
        values.forEach(value -> errorValues.add(new Object[]{value}));

        return errorValues.iterator();
    }

    /**
     * For each invalid entry of User Name, the following test iterates through all the invalid entries of password
     * and a valid password to assert expected User name error message
     *
     * @param userNames Accepts an item from the data provider for each iteration
     */
    @Test(dataProvider = "errorValues")
    void testUserNameErrorMessage(String userNames) {
        SoftAssert softAssert = new SoftAssert();

        try {
            loginPage.launchLoginPage();
            List<String> passwords = new ArrayList<>(values);
            passwords.add(PASSWORD);
            passwords.forEach(password -> {
                loginPage.loginActions(userNames, password);
                softAssert.assertEquals(loginPage.getLoginErrorMessage(), LoginPage.USERNAME_ERROR_MSG);
            });

        } finally {
            softAssert.assertAll();
        }
    }

    /**
     * With User Name set as correct value, for each invalid entry of Password,
     * the following test assert expected Password error message
     *
     * @param password Accepts an item from the data provider for each iteration
     */
    @Test(dataProvider = "errorValues")
    void testPasswordErrorMessage(String password) {
        loginPage.launchLoginPage();
        loginPage.loginActions(USERNAME, password);

        Assert.assertEquals(loginPage.getLoginErrorMessage(), LoginPage.PASSWORD_ERROR_MSG);
    }

    /**
     * Closes the browser
     * Terminates Webdriver session
     */
    @AfterClass
    void tearDown() {
        driver.close();
        driver.quit();
    }


}
