package org.example.hear.ui.modal;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.example.hear.ui.drivers.BrowserDriver;
import org.example.hear.ui.pages.ExitIntentPage;

import java.awt.*;

public class ExitIntentTest {

    private final WebDriver driver = new BrowserDriver().getDriver();


    @Test
    public void testExitIntent() throws AWTException {
        driver.manage().window().maximize();

        ExitIntentPage exitIntentPage = new ExitIntentPage(driver);
        exitIntentPage.launchExitIntentPage();
        exitIntentPage.mouseOutOfViewport();

        Assert.assertTrue(exitIntentPage.isModalDisplayed());

        exitIntentPage.clickCloseModalWindow();
    }

    @AfterClass
    void tearDown() {
        driver.close();
        driver.quit();
    }
}
