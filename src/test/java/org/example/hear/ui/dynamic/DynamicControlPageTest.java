package org.example.hear.ui.dynamic;

import org.example.hear.ui.drivers.BrowserDriver;
import org.example.hear.ui.pages.DynamicControlPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DynamicControlPageTest {

    private WebDriver driver;
    private DynamicControlPage dynamicControlPage;

    @BeforeClass
    void setup() {
        driver = new BrowserDriver().getDriver();
        dynamicControlPage = new DynamicControlPage(driver);
    }

    @Test
    void testDynamicControlPage() {
        final String testMessage = "Hear Test";

        dynamicControlPage.launchDynamicControlPage();
        dynamicControlPage.clickEnableDisableButton();
        dynamicControlPage.typeInEnabledBox(testMessage);
        dynamicControlPage.clickEnableDisableButton();
        Assert.assertEquals(dynamicControlPage.getTextFromDisabledBox(), testMessage);
    }

    @AfterClass
    void tearDown() {
        driver.close();
        driver.quit();
    }

}
