package org.example.hear.ui.tables;

import org.example.hear.ui.drivers.BrowserDriver;
import org.example.hear.ui.pages.TablesPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TablesPageTest {

    private WebDriver driver;
    private TablesPage tablesPage;
    private List<String> example1LastNames = new ArrayList<>();
    private List<String> example2FirstNames = new ArrayList<>();

    @BeforeClass
    void setup() {
        driver = new BrowserDriver().getDriver();

        tablesPage = new TablesPage(driver);
        tablesPage.launchTablesPage();

        //On page load, get Last and First name values from Table1 and Table2 respectively
        example1LastNames.addAll(tablesPage.getExample1LastNames());
        example2FirstNames.addAll(tablesPage.getExample2FirstNames());
    }

    @Test(priority = 1)
    void testLastNameSortAsc() {
        //Orders the items in ascending order
        Collections.sort(example1LastNames);

        //Clicking the 'Last Name' column header should order the table in ascending order of Last Name
        tablesPage.clickColumnHeader(true, "Last Name");
        List<String> actualSort = tablesPage.getExample1LastNames();

        Assert.assertEquals(actualSort, example1LastNames);
    }

    /**
     * This test depends on the Ascending order test, i.e, testLastNameSortAsc to first sort in ascending order,
     * and by clicking 'Last Name' column header again in this test, the table should be sorted in descending order
     */
    @Test(dependsOnMethods = "testLastNameSortAsc")
    void testLastNameSortDesc() {
        //Orders the items in descending order
        Collections.reverse(example1LastNames);

        tablesPage.clickColumnHeader(true, "Last Name");
        List<String> actualSort = tablesPage.getExample1LastNames();

        Assert.assertEquals(actualSort, example1LastNames);
    }

    @Test
    void testFirstNameSortAsc() {
        Collections.sort(example2FirstNames);

        tablesPage.clickColumnHeader(false, "First Name");
        List<String> actualSort = tablesPage.getExample2FirstNames();

        Assert.assertEquals(actualSort, example2FirstNames);
    }

    @Test(dependsOnMethods = "testFirstNameSortAsc")
    void testFirstNameSortDesc() {
        Collections.reverse(example2FirstNames);

        tablesPage.clickColumnHeader(false, "First Name");
        List<String> actualSort = tablesPage.getExample2FirstNames();

        Assert.assertEquals(actualSort, example2FirstNames);
    }

    @AfterClass
    void tearDown() {
        driver.close();
        driver.quit();
    }


}
