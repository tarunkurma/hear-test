package org.example.hear.api.data;

import java.util.List;

public class ChuckNorris {

    private List<String> categories;
    private String created_at;
    private String icon_url;
    private String id;
    private String updated_at;
    private String url;
    private String value;

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChuckNorris that = (ChuckNorris) o;

        if (!categories.equals(that.categories)) return false;
        if (!created_at.equals(that.created_at)) return false;
        if (!icon_url.equals(that.icon_url)) return false;
        if (!id.equals(that.id)) return false;
        if (!updated_at.equals(that.updated_at)) return false;
        if (!url.equals(that.url)) return false;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        int result = categories.hashCode();
        result = 31 * result + created_at.hashCode();
        result = 31 * result + icon_url.hashCode();
        result = 31 * result + id.hashCode();
        result = 31 * result + updated_at.hashCode();
        result = 31 * result + url.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ChuckNorris{" +
                "categories=" + categories +
                ", created_at=" + created_at +
                ", icon_url='" + icon_url + '\'' +
                ", id='" + id + '\'' +
                ", updated_at=" + updated_at +
                ", url='" + url + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
