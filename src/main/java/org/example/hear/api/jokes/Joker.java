package org.example.hear.api.jokes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.example.hear.api.data.Categories;
import org.example.hear.api.data.ChuckNorris;
import org.example.hear.ui.drivers.BrowserActions;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Joker {

    private static final Logger logger = Logger.getLogger(Joker.class.getName());


    public Response query(String endpoint, String queryParameter) {
        return RestAssured
                .given()
                .queryParam("query", queryParameter)
                .get(endpoint);
    }

    public List<ChuckNorris> getJokesSearch(String queryParameter) {
        Response response = query("jokes/search", queryParameter);
        if (response.getStatusCode() == HttpStatus.SC_OK) {
            return query("jokes/search", queryParameter).getBody().jsonPath().getList("result", ChuckNorris.class);
        } else {
            logger.warning("Unexpected Response with status code:" + response.getStatusCode());
            return new ArrayList<>();
        }


    }

    public Categories getJokesCategories() {
        Response response = RestAssured
                .given()
                .get("/jokes/categories");

        Categories categories = new Categories();
        categories.setCategories(response.getBody().jsonPath().get());

        return categories;

    }

}
