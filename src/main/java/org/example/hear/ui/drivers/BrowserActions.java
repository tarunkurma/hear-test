package org.example.hear.ui.drivers;

import org.apache.commons.lang3.StringUtils;
import org.example.hear.api.jokes.Joker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BrowserActions {
    private final WebDriver driver;
    private final WebDriverWait wait;
    private Properties properties = new Properties();
    private Long waitTime;

    private static final Logger logger = Logger.getLogger(BrowserActions.class.getName());

    public BrowserActions(WebDriver driver) {
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
            waitTime = Long.valueOf(properties.getProperty("wait.milliseconds"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Browser Action Error", e);
        }
        this.driver = driver;
        this.wait = new WebDriverWait(driver, waitTime);
    }

    public void getUrl(String url) {
        driver.get(url);
    }

    public boolean isDisplayed(By by) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(by)).isDisplayed();
    }


    public WebElement findPresentElement(By by) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public WebElement findClickableElement(By by) {
        return wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public WebElement findVisibleElement(By by) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public List<WebElement> findWebElements(By by) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        return driver.findElements(by);
    }


    public void type(By by, String text) {
        if (StringUtils.isBlank(text)) {
            logger.warning("WebDriver does not support 'Null' values in sendKeys function");
            return;

        }
        findPresentElement(by).sendKeys(text);
    }

    public void click(By by) {
        findClickableElement(by).click();
    }

    public String getText(By by) {
        return findVisibleElement(by).getText();
    }

    public void close() {
        driver.close();
    }

    public void quit() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return this.driver;
    }
}
