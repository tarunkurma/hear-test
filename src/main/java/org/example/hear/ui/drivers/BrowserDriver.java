package org.example.hear.ui.drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.example.hear.Constants.DRIVER_PATH;

public class BrowserDriver {

    private WebDriver driver;
    private final Properties properties = new Properties();
    private static final String OS = System.getProperty("os.name").substring(0, 3);
    private static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";

    private static final Logger logger = Logger.getLogger(BrowserDriver.class.getName());

    public BrowserDriver() {
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error loading properties file", e);
        }
        setDriver();
    }

    private void setDriver() {
        if (OS.equalsIgnoreCase("Mac")) {
            System.setProperty(WEBDRIVER_CHROME_DRIVER, DRIVER_PATH + "/chromedriver_mac");
        } else if (OS.equalsIgnoreCase("Win")) {
            System.setProperty(WEBDRIVER_CHROME_DRIVER, DRIVER_PATH + "/chromedriver_win.exe");
        } else
            throw new RuntimeException(String.format("Unsupported Operating System: %s", OS));

        driver = new ChromeDriver(setCapabilities());
    }

    private ChromeOptions setCapabilities() {
        ChromeOptions options = new ChromeOptions();
        boolean runHeadless = Boolean.parseBoolean(properties.getProperty("run.headless"));
        options.setHeadless(runHeadless);
        options.addArguments("start-maximized", "disable-infobars");
        options.addArguments("disable-popup-blocking");
        if (runHeadless) {
            options.addArguments("window-size=1200,1100");
        }
        return options;

    }

    public WebDriver getDriver() {
        return driver;
    }


}
