package org.example.hear.ui.pages;

import org.example.hear.ui.drivers.BrowserActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class TablesPage {

    private final BrowserActions actions;

    public TablesPage(WebDriver driver) {
        this.actions = new BrowserActions(driver);
    }

    public void launchTablesPage() {
        actions.getUrl("https://the-internet.herokuapp.com/tables#edit");
    }

    public void clickColumnHeader(boolean isExample1, String columnName) {
        if (isExample1) {
            actions.click(By.xpath(String.format("//span[text()='%s']", columnName)));
        } else actions.click(By.xpath(String.format("(//span[text()='%s'])[2]", columnName)));
    }

    public List<String> getExample1LastNames() {
        return getColumnValues("table1", 1);
    }

    public List<String> getExample2FirstNames() {
        return getColumnValues("table2", 2);
    }


    private List<String> getColumnValues(String tableId, int columnId) {
        List<String> values = new ArrayList<>();
        int rowSize = actions.findWebElements(By.xpath(String.format("//table[@id='%s']/tbody[1]/tr", tableId))).size();

        for (int i = 1; i <= rowSize; i++) {
            values.add(actions.getText(By.xpath(String.format("//table[@id='%s']/tbody[1]/tr[%s]/td[%s]", tableId, i, columnId))));
        }

        return values;
    }

}
