package org.example.hear.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.example.hear.ui.drivers.BrowserActions;

import java.awt.*;

public class ExitIntentPage {

    private final BrowserActions actions;

    public ExitIntentPage(WebDriver driver) {
        this.actions = new BrowserActions(driver);

    }

    public void launchExitIntentPage() {
        actions.getUrl("http://the-internet.herokuapp.com/exit_intent");
    }

    public boolean isExitIntentHeaderDisplayed() {
        return actions.isDisplayed(By.xpath("//h3[text()='Exit Intent']"));
    }

    public void mouseOutOfViewport() throws AWTException {
        //TODO: implement better solution than using Robot API
        new Robot().mouseMove(0, -1);
    }


    public boolean isModalDisplayed() {
        return actions.isDisplayed(By.className("modal-title"));
    }

    public void clickCloseModalWindow() {
        actions.click(By.xpath("//div[@id='ouibounce-modal']/div[2]/div[3]/p[1]"));
    }
}
