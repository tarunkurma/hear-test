package org.example.hear.ui.pages;

import org.example.hear.ui.drivers.BrowserActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WindowsPage {

    private final BrowserActions actions;
    private final WebDriver driver;

    public WindowsPage(WebDriver driver) {
        this.driver = driver;
        this.actions = new BrowserActions(driver);
    }

    public void launchWindowsPage() {
        actions.getUrl("https://the-internet.herokuapp.com/windows");
    }

    public void clickHere() {
        actions.click(By.xpath("//a[@href='/windows/new']"));
    }

    public void switchToNewPage() {
        driver.getWindowHandles().forEach(handle -> driver.switchTo().window(handle));

    }

    public String getNewPageHeaderText() {
        return actions.getText(By.tagName("h3"));
    }
}
