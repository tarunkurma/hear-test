package org.example.hear.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.example.hear.ui.drivers.BrowserActions;

public class LoginPage {

    public static final String USERNAME_ERROR_MSG = "Your username is invalid!";
    public static final String PASSWORD_ERROR_MSG = "Your password is invalid!";

    private final BrowserActions actions;

    public LoginPage(WebDriver driver) {
        actions = new BrowserActions(driver);
    }


    public void launchLoginPage() {
        actions.getUrl("https://the-internet.herokuapp.com/login");
    }

    private void typeUsername(String username) {
        actions.type(By.id("username"), username);
    }

    private void typePassword(String password) {
        actions.type(By.id("password"), password);
    }

    private void clickLogin() {
        actions.click(By.xpath("//button[@type='submit']"));
    }

    public String getLoginErrorMessage() {
        return actions.getText(By.id("flash")).substring(0,25);
    }

    public void loginActions(String username, String password) {
        typeUsername(username);
        typePassword(password);
        clickLogin();
    }


}
