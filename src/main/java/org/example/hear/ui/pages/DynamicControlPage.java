package org.example.hear.ui.pages;

import org.example.hear.ui.drivers.BrowserActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DynamicControlPage {

    private final BrowserActions actions;

    public DynamicControlPage(WebDriver driver) {
        this.actions = new BrowserActions(driver);
    }

    public void launchDynamicControlPage() {
        actions.getUrl("https://the-internet.herokuapp.com/dynamic_controls");
    }

    public String getMessageText() {
        return actions.findVisibleElement(By.id("message")).getText();
    }

    public void clickEnableDisableButton() {
        actions.click(By.xpath("//form[@id='input-example']//button[1]"));
    }

    public void typeInEnabledBox(String text) {
        if (getMessageText().equalsIgnoreCase("It's enabled!")) {
            actions.type(By.xpath("//form[@id='input-example']//input[1]"), text);
        }
    }

    public String getTextFromDisabledBox() {
        if (getMessageText().equalsIgnoreCase("It's disabled!")) {
            return actions.findVisibleElement(By.xpath("//form[@id='input-example']//input[1]")).getAttribute("value");
        } else return null;
    }


}
