package org.example.hear.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.example.hear.ui.drivers.BrowserActions;

public class SecureAreaPage {

    private BrowserActions actions;
    public static final String SECURE_MSG = "Welcome to the Secure Area. When you are done click logout below.";

    public SecureAreaPage(WebDriver driver) {
        actions = new BrowserActions(driver);
    }

    public String getSecureAreaMessage() {
        return actions.getText(By.className("subheader"));

    }

    public void logout() {
        actions.click(By.xpath("//a[@href='/logout']"));
    }


}
