# hear-test
   

#### Project Specifications

| Software      | Version  |
|---------------|----------|
| Java          | 15.0.1   |
| Apache Maven  | 3.6.3    |
| TestNG        | 7.3.0    |
| Selenium      | 3.141.59 |
| RestAssured   | 4.3.3    |

#### System Specifications 
This project was developed using:

| OS                | Browser             |
|-------------------|---------------------|
| macOS BigSur 11.1 | Chrome 88.0.4324.96 |



#### Test Suites

* API Tests 
* UI Tests 
* All Tests

> The design goal for this project was to create
> atomic, data independent tests that are easy to 
> read, maintain and scale. 
>
> The tests can be run individually, at a class level, 
> or as an entire suite, in parallel.
> 
> Classes, Methods, Variables and Tests were named in a manner to 
> reduce the need for additional documentation.

### Run Tests

API Tests
```sh
mvn clean test -DtestSuite=api_testng.xml
```

UI Tests
```sh
mvn clean test -DtestSuite=ui_testng.xml
```

All tests:
```sh
mvn clean test 
```

#### Test Report

Surefire Report is generated in `/target` folder:

````$xslt
/target/surefire-reports/index.html
or
/target/surefire-reports/emailable-report.html
````

